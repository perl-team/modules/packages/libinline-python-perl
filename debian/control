Source: libinline-python-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>,
           tony mancill <tmancill@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: chrpath,
               debhelper-compat (= 13),
               libinline-perl <!nocheck>,
               libjson-perl <!nocheck>,
               libproc-processtable-perl <!nocheck>,
               libtest-deep-perl <!nocheck>,
               libtest-number-delta-perl <!nocheck>,
               perl-xs-dev,
               perl:native,
               python3-dev
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libinline-python-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libinline-python-perl.git
Homepage: https://metacpan.org/release/Inline-Python
Rules-Requires-Root: no

Package: libinline-python-perl
Architecture: any
Depends: ${misc:Depends},
         ${perl:Depends},
         ${shlibs:Depends},
         libinline-perl
Description: module to write Perl subs and classes in Python
 The Inline::Python module allows one to write Perl subroutines in Python. One
 doesn't have to use any funky techniques for sharing most types of data
 between the two languages, either. Inline::Python comes with its own data
 translation service. It converts any Python structures it knows about into
 Perl structures, and vice versa.
 .
 Inline::Python sets up an in-process Python interpreter, runs the code, and
 then examines Python's symbol table for things to bind to Perl. The process
 of interrogating the Python interpreter for globals only occurs the first
 time the Python code is run. The namespace is cached, and subsequent calls
 use the cached version.
